package cn.com.bo.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSource;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import cn.com.bo.util.excel.ExportExcel;
import cn.com.bo.util.mysql.MysqlUtil;


/**
 * 入口执行类
 * 执行命令：java -cp export-excel-0.0.1-SNAPSHOT.jar cn.com.bo.export.App
 *  -u root 
 *  -p 123456
 *  -url 121.0.0.1:3306/dsp_new 
 *  -sql "select username,password from user;" 
 *  -pa export8.xls
 * @author ZhangShaobo
 * @date 2017-09-20
 */
public class App 
{
	
	/**
	 * @param args
	 */
    public static void main( String[] args )
    {
    	
    	AdxArg adxArg = new AdxArg();
    	CmdLineParser p = new CmdLineParser(adxArg);
		try {
			p.parseArgument(args);
		} catch (CmdLineException e) {
			e.printStackTrace();
		}
		exportExcel(adxArg);
    }
    
    public static void exportExcel(AdxArg adxArg){
    	BasicDataSource ds = new BasicDataSource();
		ds.setUsername(adxArg.username);
		ds.setPassword(adxArg.password);
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://"+adxArg.url+"?useUnicode=true&characterEncoding=UTF-8&useOldAliasMetadataBehavior=true");
    	MysqlUtil.setDataSource(ds);
    	List<Map<String, String>> list = MysqlUtil.exeSql(adxArg.sql);
    	FileOutputStream fileout = null;
		try {
			fileout = new FileOutputStream(new File(adxArg.path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	ExportExcel.exportExcel("qwe", list, fileout, "yyyy-MM-dd");
    	
    }
    
    
}

class AdxArg{
	@Option(name = "-u", required = true, usage = "mysql username")
	public String username;
	
	@Option(name = "-p", required = true, usage = "mysql password")
	public String password;
	
	@Option(name = "-url", required = true, usage = "mysql url, ip:port/database")
	public String url;

	@Option(name = "-sql", required = true, usage = "sql")
	public String sql;
	
	@Option(name = "-pa", required = true, usage = "excel path")
	public String path;
	
}
