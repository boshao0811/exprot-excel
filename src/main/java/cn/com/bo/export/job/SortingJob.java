package cn.com.bo.export.job;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import cn.com.bo.export.util.ReadExcel;
import cn.com.bo.util.excel.ExportExcel;

public class SortingJob {
	public static void main(String[] args) {
		String filePath = "/Users/lmm/Desktop/北京乐人固定资产明细表（行政表）.xlsx";
		Integer titleIndex = 2;
		Integer[] sheets = {1,2,3};
		String sortingField = "存放地点";
		
		String resultPath = filePath.substring(0, filePath.lastIndexOf("/"));
		System.out.println(resultPath);
		List<Map<String, String>> list = ReadExcel.readExcel(filePath, titleIndex, sheets);
		Collections.sort(list, new Comparator<Map<String, String>>() {
            public int compare(Map<String, String> o1, Map<String, String> o2) {
                String name1 = o1.get(sortingField).toString(); 
                String name2 = o2.get(sortingField).toString(); 
                return name1.compareTo(name2);
            }
        });
		String sortValue = list.get(0).get(sortingField);
		List<Map<String, String>> result = new ArrayList<>();
		for (Map<String, String> m : list) {
			
			System.out.println(m.get(sortingField));
			if (!sortValue.equals(m.get(sortingField))) {
				if (!sortValue.equals("")) {
					try {
						FileOutputStream fileOutputStream = new FileOutputStream(new File(resultPath+"/科技馆_"+sortValue+".xls"));
						ExportExcel.exportExcel(sortValue, result, fileOutputStream, "");
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
				sortValue = m.get(sortingField);
				result.clear();
			} 
			result.add(m);
		}
	}
	
	
}
